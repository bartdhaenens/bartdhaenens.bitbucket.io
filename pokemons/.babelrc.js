module.exports = {
    presets: [
      [
        require.resolve('@babel/preset-env'),
        {
          targets: {
            browsers: [
              'last 2 versions',
              'safari >= 8',
              'ie >= 10',
              'Android >= 4.4.2'
            ]
          },
          loose: true,
          useBuiltIns: 'usage',
          modules: false
        }
      ],
      require.resolve('@babel/preset-react')
    ],
    plugins: [
      require.resolve('@babel/plugin-external-helpers'),
      require.resolve('@babel/plugin-transform-runtime'),
      require.resolve('@babel/plugin-proposal-class-properties'),
      require.resolve('@babel/plugin-transform-react-jsx'),
      require.resolve('@babel/plugin-proposal-object-rest-spread'),
      require.resolve('babel-plugin-styled-components')
    ]
  }
  