module.exports = {
    env: {
      browser: true,
      node: true,
      es6: true
    },
    extends: ['eslint:recommended', 'plugin:react/recommended', 'prettier'],
    parser: 'babel-eslint',
    parserOptions: {
      ecmaVersion: 2017,
      sourceType: 'module',
      ecmaFeatures: {
        jsx: true
      }
    },
    plugins: ['babel', 'react', 'jsx-a11y', 'import', 'prettier'],
    rules: {
      'prettier/prettier': [
        'error',
        {
          singleQuote: true,
          bracketSpacing: true,
          semi: true
        }
      ],
      'react/prop-types': 0
    }
  }
  