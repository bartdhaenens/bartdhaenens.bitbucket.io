import styled from 'styled-components';

export const FlexContainerRow = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: no-wrap;
  width: 100%;
  padding-left: 10px;
`;

export const FlexContainerCollumn = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: no-wrap;
  padding-left: 10px;
  width: ${props => props.collumnWidth || 100}%;
`;

export const AppContainer = styled(FlexContainerRow)`
  max-width: 980px;
  margin: 0 auto;
`;

export const AppHeader = styled.img.attrs({
  src:
    'https://upload.wikimedia.org/wikipedia/commons/9/98/International_Pokémon_logo.svg',
  alt: 'Pokemon'
})`
  display: block;
  margin: 0 auto;
  width: 200px;
  margin-bottom: 30px;
`;
