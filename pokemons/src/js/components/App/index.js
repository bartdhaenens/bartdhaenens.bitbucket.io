import React from 'react';
import _ from 'lodash';
import { AppContainer, AppHeader } from '../../common/styles';
import PokemonList from '../PokemonList';
import PokemonHighlight from '../PokemonHighlight';
import PokemSelected from '../PokemonSelected';

class App extends React.Component {
  state = {
    pokemons: [],
    highlighted: null,
    squad: []
  };
  static defaultProps = {
    url: 'https://pokeapi.co/api/v2/pokemon/'
  };

  addPokemonToSquad = pokemon => {
    const { squad } = this.state;
    if (!pokemon.moves.length) {
      alert('Please select at least 1 move from the right panel.');
      return;
    }

    if (squad.length < 6) {
      const updatedSquad = [...squad, pokemon];
      this.setState({
        squad: updatedSquad
      });
    }
  };

  componentDidMount() {
    const { url } = this.props;
    return fetch(url)
      .then(res => res.json())
      .then(data => {
        const orderedData = _.orderBy(data.results, 'name');
        this.setState({
          pokemons: orderedData
        });
      })
      .catch(err => {
        // eslint-disable-next-line
        console.log(err);
      });
  }

  render() {
    const { pokemons, highlighted, squad } = this.state;
    return (
      <React.Fragment>
        <AppHeader />
        <AppContainer>
          <PokemonList
            pokemons={pokemons}
            handleSelect={select => this.setState({ highlighted: select })}
            {...this.props}
          />
          <PokemonHighlight
            highlighted={highlighted}
            addPokemon={pokemon => this.addPokemonToSquad(pokemon)}
            {...this.props}
          />
        </AppContainer>
        <PokemSelected squad={squad} />
      </React.Fragment>
    );
  }
}

export default App;
