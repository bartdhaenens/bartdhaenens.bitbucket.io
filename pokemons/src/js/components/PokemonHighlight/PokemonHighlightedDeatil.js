import React from 'react';
import {
  HighlightedImage,
  HighlightedName,
  HighlightedSaveButton
} from './styles';

class PokemonHighlightedDetail extends React.Component {
  static defaultProps = {
    addPokemon: () => {}
  };

  render() {
    const { imgUrl, name, addPokemon, type, selectedMoves } = this.props;
    return (
      <React.Fragment>
        <HighlightedImage imgUrl={imgUrl} />
        <HighlightedName>{name}</HighlightedName>
        <HighlightedSaveButton
          onClick={() =>
            addPokemon({
              name,
              img: imgUrl,
              type,
              moves: selectedMoves
            })
          }
        >
          Save Pokemon
        </HighlightedSaveButton>
      </React.Fragment>
    );
  }
}

export default PokemonHighlightedDetail;
