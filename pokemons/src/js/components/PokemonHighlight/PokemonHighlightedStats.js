import React from 'react';
import {
  HighlightedStatsHeading,
  HighlightedStatsWrapper,
  HighlightedStatsItem,
  HighlightedStatName,
  HighlightedStatValue,
  HighlightedSelectedMoveWrapper,
  HighlightedSelectedMoveItem,
  HighlightedSelectedMoveName,
  HighlightedSelectedMoveMethod
} from './styles';

class HighlightedStats extends React.Component {
  render() {
    const { stats, selectedMoves } = this.props;
    return (
      <React.Fragment>
        <HighlightedStatsHeading>Stats</HighlightedStatsHeading>
        <HighlightedStatsWrapper>
          {stats.map((stat, index) => (
            <HighlightedStatsItem key={index}>
              <HighlightedStatName>{stat.stat.name}</HighlightedStatName>
              <HighlightedStatValue>{stat.base_stat}</HighlightedStatValue>
            </HighlightedStatsItem>
          ))}
        </HighlightedStatsWrapper>
        <HighlightedStatsHeading>Selected Moves</HighlightedStatsHeading>
        {selectedMoves ? (
          <HighlightedSelectedMoveWrapper>
            {selectedMoves.map(selected => (
              <HighlightedSelectedMoveItem key={selected.move}>
                <HighlightedSelectedMoveMethod>
                  {selected.method}
                </HighlightedSelectedMoveMethod>
                <HighlightedSelectedMoveName>
                  {`${selected.move
                    .charAt(0)
                    .toUpperCase()}${selected.move.slice(1)}`}
                </HighlightedSelectedMoveName>
              </HighlightedSelectedMoveItem>
            ))}
          </HighlightedSelectedMoveWrapper>
        ) : null}
      </React.Fragment>
    );
  }
}

export default HighlightedStats;
