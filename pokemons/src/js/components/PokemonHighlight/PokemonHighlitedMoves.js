import React from 'react';
import _ from 'lodash';
import {
  HighlightedSelectMoveHeader,
  HighlightedSelectMoveList,
  HighlightedSelectMoveListItem,
  HighlightedSelectMoveListEmpty
} from './styles';

class PokemonHighLightedMoves extends React.Component {
  state = {
    selectableMoves: {},
    selectedMethod: null,
    selectedMoves: []
  };

  static defaultProps = {
    setSelectedMoves: () => {}
  };

  filterMoves = moves => {
    let names = [];
    let grouped = [];
    moves.map(move => {
      names.push({
        name: move.move.name,
        method: move.version_group_details[0].move_learn_method.name
      });
    });
    grouped = _.groupBy(names, name => name.method);
    return grouped;
  };

  selectMove = ({ move, method }) => {
    const currentSelected = this.state.selectedMoves;
    if (
      !_.find(currentSelected, { move }) &&
      currentSelected.length <= 3 &&
      currentSelected.filter(selected => selected.method === method).length < 1
    ) {
      currentSelected.push({
        move,
        method
      });
    } else if (_.find(currentSelected, { move })) {
      _.remove(currentSelected, moves => moves.move === move);
    }

    this.setState({
      selectedMoves: currentSelected
    });
  };

  componentDidMount() {
    this.setState({
      selectableMoves: this.filterMoves(this.props.moves)
    });
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps.moves) !== JSON.stringify(this.props.moves)) {
      this.setState(
        {
          selectableMoves: this.filterMoves(this.props.moves),
          selectedMethod: null,
          selectedMoves: []
        },
        () => this.props.setSelectedMoves(this.state.selectedMoves)
      );
    }
  }

  render() {
    const { selectableMoves, selectedMethod, selectedMoves } = this.state;
    const { setSelectedMoves } = this.props;
    return (
      <React.Fragment>
        <HighlightedSelectMoveHeader>
          {Object.keys(selectableMoves).map(method => (
            <span
              key={method}
              onClick={() => this.setState({ selectedMethod: method })}
            >
              {method}
            </span>
          ))}
        </HighlightedSelectMoveHeader>
        <HighlightedSelectMoveList>
          {selectedMethod ? (
            <React.Fragment>
              {selectableMoves[selectedMethod].map(move => (
                <HighlightedSelectMoveListItem
                  key={move.name}
                  onClick={() => {
                    this.selectMove({ move: move.name, method: move.method });
                    setSelectedMoves(selectedMoves);
                  }}
                >
                  {`${move.name.charAt(0).toUpperCase()}${move.name.slice(1)}`}
                </HighlightedSelectMoveListItem>
              ))}
            </React.Fragment>
          ) : (
            <HighlightedSelectMoveListEmpty>
              Select a group first
            </HighlightedSelectMoveListEmpty>
          )}
        </HighlightedSelectMoveList>
      </React.Fragment>
    );
  }
}

export default PokemonHighLightedMoves;
