import React from 'react';
import _ from 'lodash';
import { FlexContainerCollumn, FlexContainerRow } from '../../common/styles';
import { HighlightedPlaceholder } from './styles';
import PokemonHighlightedStats from './PokemonHighlightedStats';
import PokemonHighlightedDetail from './PokemonHighlightedDeatil';
import PokemonHighlightedMoves from './PokemonHighlitedMoves';

class PokemonHighlight extends React.Component {
  state = {
    selectedMoves: []
  };

  static defaultProps = {
    addPokemon: () => {}
  };

  getType = () => {
    const { highlighted } = this.props;
    return _.find(highlighted.types, { slot: 1 }).type.name;
  };

  componentDidUpdate() {}

  render() {
    const { highlighted } = this.props;
    const { selectedMoves } = this.state;
    return (
      <FlexContainerCollumn collumnWidth={80}>
        {!highlighted && (
          <HighlightedPlaceholder>
            Select a Pokémon from the list on the left
          </HighlightedPlaceholder>
        )}
        {highlighted && (
          <FlexContainerRow>
            <FlexContainerCollumn collumnWidth={20}>
              <PokemonHighlightedDetail
                imgUrl={highlighted.sprites.front_default}
                name={highlighted.name}
                type={this.getType()}
                selectedMoves={selectedMoves}
                {...this.props}
              />
            </FlexContainerCollumn>
            <FlexContainerCollumn collumnWidth={50}>
              <PokemonHighlightedStats
                stats={highlighted.stats}
                selectedMoves={selectedMoves}
              />
            </FlexContainerCollumn>
            <FlexContainerCollumn collumnWidth={30}>
              <PokemonHighlightedMoves
                moves={highlighted.moves}
                setSelectedMoves={selectedMoves =>
                  this.setState({ selectedMoves })
                }
              />
            </FlexContainerCollumn>
          </FlexContainerRow>
        )}
      </FlexContainerCollumn>
    );
  }
}

export default PokemonHighlight;
