import styled from 'styled-components';

export const HighlightedPlaceholder = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: #c1c8ca;
  height: 100%;
`;

export const HighlightedImage = styled.img.attrs({
  src: props => props.imgUrl
})`
  width: 96px;
  height: 96px;
  display: block;
`;

export const HighlightedName = styled.h2`
  text-transform: uppercase;
  font-size: 18px;
  font-weight: bold;
  color: #3a5d9f;
`;

export const HighlightedSaveButton = styled.div`
  background-color: #3a5d9f;
  color: white;
  text-transform: uppercase;
  cursor: pointer;
  font-size: 13px;
  text-align: center;
  padding: 7px 14px;
`;

export const HighlightedStatsHeading = styled.h3`
  text-align: center;
  text-transform: uppercase;
  font-size: 10px;
  margin: 6px 0px 3px;
  color: #3a5d9f;
`;

export const HighlightedStatsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-self: flex-start;
  margin-bottom: 10px;
`;

export const HighlightedStatsItem = styled.div`
  width: 50%;
  text-align: right;
`;

export const HighlightedStatName = styled.span`
  font-size: 12px;
  text-transform: uppercase;
  margin-right: 5px;
  color: #fdcc07;
`;

export const HighlightedStatValue = styled.span`
  font-size: 30px;
  color: #3a5d9f;
`;

export const HighlightedSelectMoveHeader = styled.div`
  text-align: center;
  span {
    margin-right: 5px;
    color: #fdcc07;
    text-transform: uppercase;
    font-size: 12px;
    cursor: pointer;
  }
`;

export const HighlightedSelectMoveList = styled.div`
  max-height: 210px;
  overflow: auto;
`;

export const HighlightedSelectMoveListEmpty = styled.div`
  text-align: center;
  text-transform: uppercase;
  font-size: 10px;
  color: #c1c8ca;
`;

export const HighlightedSelectMoveListItem = styled.div`
  cursor: pointer;
  font-size: 12px;
  color: #3a5d9f;
`;

export const HighlightedSelectedMoveWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;
export const HighlightedSelectedMoveItem = styled.div`
  width: 49%;
  box-sizing: border-box;
  padding: 5px;
  border: 1px solid #3a5d9f;
  margin-bottom: 5px;
`;

export const HighlightedSelectedMoveName = styled.div`
  color: #3a5d9f;
`;

export const HighlightedSelectedMoveMethod = styled.div`
  color: #fdcc07;
  font-size: 10px;
  text-transform: uppercase;
`;
