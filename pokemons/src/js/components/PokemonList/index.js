import React from 'react';
import { Header, SelectList, SelectListItem, SelectListInput } from './styles';
import { FlexContainerCollumn } from '../../common/styles';

class PokemonList extends React.Component {
  state = {
    selected: null,
    pokemons: []
  };

  static defaultProps = {
    handleSelect: () => {}
  };

  componentDidMount() {
    const { pokemons } = this.props;
    this.setState({
      pokemons
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.pokemons !== this.props.pokemons) {
      this.setState({
        pokemons: this.props.pokemons
      });
    }
  }

  handleClick = fetchUrl => {
    fetch(fetchUrl)
      .then(res => res.json())
      .then(data => {
        this.setState({
          selected: data
        });
        this.props.handleSelect(data);
      });
  };

  handleChange = evt => {
    const { pokemons } = this.props;
    const result = pokemons.filter(
      pokemon => pokemon.name.search(evt.target.value.toLowerCase()) !== -1
    );
    this.setState({
      pokemons: result
    });
  };

  render() {
    const { pokemons } = this.state;
    return (
      <FlexContainerCollumn collumnWidth={20}>
        <Header>Select a Pokemon</Header>
        <SelectListInput onChange={this.handleChange} />
        <SelectList>
          {pokemons.map(pokemon => (
            <SelectListItem
              key={pokemon.name}
              onClick={() => {
                this.handleClick(pokemon.url);
              }}
            >
              {pokemon.name}
            </SelectListItem>
          ))}
        </SelectList>
      </FlexContainerCollumn>
    );
  }
}

export default PokemonList;
