import styled from 'styled-components';

export const Header = styled.div`
  text-transform: uppercase;
  text-align: center;
  color: #fdcc07;
  font-size: 15px;
  font-weight: 600;
`;

export const SelectList = styled.div`
  height: 200px;
  overflow-y: auto;
`;

export const SelectListItem = styled.div`
  background-color: #3a5d9f;
  color: #fdcc07;
  text-transform: uppercase;
  font-size: 14px;
  cursor: pointer;
  padding: 7px 14px;
  border-radius: 10px;
  margin: 4px 0px;
`;

export const SelectListInput = styled.input.attrs({
  type: 'text',
  placeholder: 'type to filter'
})`
  outline: none;
  width: 100%;
  box-sizing: border-box;
  border: 3px solid #3a5d9f;
  padding: 5px;
  text-transform: uppercase;
  margin-bottom: 5px;
`;
