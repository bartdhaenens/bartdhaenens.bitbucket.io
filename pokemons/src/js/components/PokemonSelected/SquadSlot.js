import React from 'react';
import {
  SquadEmptySlot,
  SquadPlaceholder,
  SquadPokemonSlotImg,
  SquadPokemonSlotName,
  SquadPokemonSlotMove
} from './styles';

class SquadSlot extends React.Component {
  shouldComponentUpdate(nextProps) {
    if (this.props.pokemon === nextProps.pokemon) {
      return false;
    }
    return true;
  }

  render() {
    const { pokemon, handleClick } = this.props;
    return (
      <SquadPlaceholder
        type={pokemon ? pokemon.type : null}
        collumnWidth={100 / 6}
        onClick={pokemon ? handleClick : () => {}}
      >
        {pokemon ? (
          <React.Fragment>
            <SquadPokemonSlotImg imgUrl={pokemon.img} />
            <SquadPokemonSlotName>{pokemon.name}</SquadPokemonSlotName>
            {pokemon.moves.map((move, index) => (
              <SquadPokemonSlotMove key={index}>
                {move.move}
              </SquadPokemonSlotMove>
            ))}
          </React.Fragment>
        ) : (
          <SquadEmptySlot>empty</SquadEmptySlot>
        )}
      </SquadPlaceholder>
    );
  }
}

export default SquadSlot;
