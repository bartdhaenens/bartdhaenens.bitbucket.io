import React from 'react';
import _ from 'lodash';
import { FlexContainerRow } from '../../common/styles';
import SquadSlot from './SquadSlot';
import { SquadHeader, SquadWrapper } from './styles';

class PokemonSelected extends React.Component {
  state = {
    squad: []
  };

  componentDidMount() {
    this.setState({
      squad: this.props.squad
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.squad !== this.props.squad) {
      this.setState({
        squad: this.props.squad
      });
    }
  }

  removeFromSquad = slot => {
    const { squad } = this.props;
    const updatedSquad = squad.splice(slot, 1);
    this.setState({
      squad: updatedSquad
    });
  };

  render() {
    const { squad } = this.props;
    return (
      <React.Fragment>
        <FlexContainerRow>
          <SquadHeader>Selected squad</SquadHeader>
        </FlexContainerRow>
        <FlexContainerRow>
          <SquadWrapper {...this.props}>
            {_.times(6, index => (
              <SquadSlot
                key={index}
                pokemon={squad[index]}
                handleClick={() => this.removeFromSquad(index)}
              />
            ))}
          </SquadWrapper>
        </FlexContainerRow>
      </React.Fragment>
    );
  }
}

export default PokemonSelected;
