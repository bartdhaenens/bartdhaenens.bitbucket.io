import styled from 'styled-components';

const types = {
  normal: '#A8A77A',
  psychic: '#F95587',
  fire: '#EE8130',
  water: '#6390F0',
  electric: '#F7D02C',
  grass: '#7AC74C',
  ice: '#96D9D6',
  fighting: '#C22E28',
  poison: '#A33EA1',
  ground: '#E2BF65',
  flying: '#A98FF3',
  bug: '#A6B91A',
  rock: '#B6A136',
  ghost: '#735797',
  dragon: '#6F35FC',
  dark: '#705746',
  steel: '#B7B7CE',
  fairy: '#D685AD'
};

export const SquadWrapper = styled.div`
  display: flex;
  width: 100%;
  max-width: 980px;
  height: 250px;
  margin: 0 auto;
  margin-bottom: 50px;
`;

export const SquadPlaceholder = styled.div`
  background-color: ${props => types[props.type] || '#c1c8ca'};
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  width: ${100 / 6}%;
  justify-content: center;
  cursor: pointer;
  margin: 0px 1%;
  padding: 20px;
`;

export const SquadEmptySlot = styled.div`
  font-size: 10px;
  text-transform: lowercase;
  text-align: center;
  color: #3a5d9f;
`;

export const SquadPokemonSlotImg = styled.img.attrs({
  src: props => props.imgUrl
})`
  width: 96px;
  height: 96px;
  display: block;
`;

export const SquadPokemonSlotName = styled.div`
  color: white;
  text-transform: uppercase;
  font-size: 12px;
  font-weight: 600;
  max-width: 100%;
  white-space: nowrap;
  text-overflow: ellipsis;
  text-align: center;
  margin-bottom: 4px;
  overflow: hidden;
`;

export const SquadPokemonSlotMove = styled.div`
  background-color: white;
  color: #3a5d9f;
  font-size: 10px;
  margin-bottom: 5px;
  padding: 3px;
  border-radius: 5px;
`;

export const SquadHeader = styled.div`
  font-size: 15px;
  font-weight: 600;
  text-transform: uppercase;
  width: 100%;
  text-align: center;
  color: #fdcc07;
  margin-top: 15px;
  margin-bottom: 6px;
`;
