const { resolve } = require('path');
const rootDir = process.cwd();
const webpack = require('webpack');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const TARGET = process.env.npm_lifecycle_event;

const config = {
  context: resolve(rootDir, 'src'),
  entry: ['./js/index.js'],
  output: {
    filename: 'js/index.js',
    path: resolve(rootDir, 'dist'),
    publicPath: ''
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: [/node_modules/],
        use: [
          {
            loader: 'babel-loader'
          }
        ]
      }
    ]
  },
  plugins: []
};

if (TARGET === 'start') {
  module.exports = merge(config, {
    mode: 'development',
    entry: [
      'react-hot-loader/patch',
      'webpack-dev-server/client?http://localhost:8000',
      'webpack/hot/only-dev-server'
    ],
    devServer: {
      hot: true,
      inline: true,
      port: 8000,
      contentBase: resolve(rootDir, 'build'),
      publicPath: '/',
      open: true,
      https: false,
      stats: {
        modules: false,
        assets: true,
        publicPath: true,
        children: false
      }
    },
    plugins: [new webpack.HotModuleReplacementPlugin()]
  });
}

if (TARGET === 'build') {
  module.exports = merge(config, {
    mode: 'production',
    plugins: [
      new CleanWebpackPlugin('dist', { root: rootDir, verbose: false }),
      new HtmlWebpackPlugin({
        filename: resolve(rootDir, 'dist/index.html'),
        template: resolve(rootDir, 'src/html/index.html'),
        inject: false,
        title: "Gotta catch 'em all",
        minify: {
          collapseWhitespace: true
        }
      })
    ]
  });
}
